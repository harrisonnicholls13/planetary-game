# Planetary simulation game
By Harrison Nicholls  
Inspired by Kim Stanley Robinson's *Mars* book series  

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

## Game requirements
* Linux
* GCC
* Make
* SDL2
* SDL2_ttf
* SDL2_mixer
* SDL2_image

## Running the game
Once compiled, simply run the binary in `./bin/`. It will automatically look for a config file in `./res/default.conf`, or you can provide one as an argument to the binary at runtime. If neither of these are found, it will resort to predefined values.

## Overview
* Top down game focused on planetary/societal evolution and strategy.
* Main display is a map of a planet (Mars?) where you can view settlements, roads, trains, etc.
* Must allow you to zoom in on parts of planet without loading screen (use mip mapping).
* Map is not simply a 2D object - should simulate strata in rock, dynamic water level, different biomes, etc. Weather is (inaccurately) simulated, to allow for terraforming. Core feature of game is that the land is entirely dynamic. Even though game appears top-down, the z-axis is very important.
* Because game takes place on planet-wide (or solar-system-wide) scale, must use CPU players to make the universe feel alive. They automatically build cities, etc.
* Avoid micromanagement of cities etc
* Game is more of a simluation, than a typical game

## Inspirations
* Kim Stanley Robinson
	* Overall vibe of the universe
	* Story model (technology, revolution, society, exploration, politics)
	* Atmospheric simulations (for habitability, terraforming, climate change, colour scheme, etc)
* Rimworld
	* Variable terrain
	* CPU societies
* Civ
	* Politics
	* Events
	* Map view
* Stellaris
	* Wide political spectrum
	* Evolving technologies, not just upgrades
* OpenTTD
	* Travel networks
* Minecraft / Terraria
	* Dynamic terrain
	* Biomes
	* Automatic terrain generation (do preset terrains first, like real Mars or real Earth)
	* Water physics

* Consider: https://gdal.org/programs/gdaldem.html
* Consider: https://gamedev.stackexchange.com/questions/89824/how-can-i-compute-a-steepness-value-for-height-map-cells (sobel filter)


## Resource credits

* Fergason, R.L, Hare, T.M., & Laura, J. (2018). HRSC and MOLA Blended Digital Elevation Model at 200m v2. Astrogeology PDS Annex, U.S. Geological Survey. http://bit.ly/HRSC_MOLA_Blend_v0

* Kevin MacLeod (incompetech.com) Licensed under Creative Commons: By Attribution 4.0 License.   
Pieces used: Rising Tide, Limit, Finding the Balance, Voltaic, Beauty Flow