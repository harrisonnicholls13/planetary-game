#include "planetary.h"
#include "globalvars.h"
#include "gamevars.h"

// Objects

pixelarray_t*	ingame_pixelarrays_relief[CHUNKS_NY*CHUNKS_NX];
char*			ingame_pixelarrays_relief_path = "./res/planet_maps/mars/gen_relief/";

pixelarray_t*	ingame_pixelarrays_dem[CHUNKS_NY*CHUNKS_NX];
char*			ingame_pixelarrays_dem_path = "./res/planet_maps/mars/gen_dem/";

texture_t*		ingame_minimap_chunks[CHUNKS_NX*CHUNKS_NY];

rectangle_t*	ingame_minimap_back;
rectangle_t*	ingame_minimap_front;
rectangle_t*	ingame_minimap_border;

cmap_t 			cmap;

// Variables
float			cameraX;
float 			cameraY;
int				cameraZ = 0;
int				cameraZ_old = 0;
bool 			hasMoved = false;
int				idx_of_order0;
float			cw;	// Width of 1 chunk
float			ch;	// Height ''
float 			fw; // Width of all chunks summed up
float 			fh; // Height ''
float 			cx_initial[CHUNKS_NX*CHUNKS_NY];
float 			cy_initial[CHUNKS_NX*CHUNKS_NY];

// Layers arrangement
// 0 		Distance
// 1		Image background
// 10--19	PixelArrays and main map
// 20--29	Interface
// 30--39	Pause menu?

bool gsf_ingame_init() {

	printf(PRT_MSG"Waiting for PixelArray threads to finish \n");

	for (int i = 0; i < CHUNKS_NY*CHUNKS_NX; i++) {
		SDL_WaitThread(ingame_pixelarrays_relief[i]->thread, NULL);
		SDL_WaitThread(ingame_pixelarrays_dem[i]->thread, NULL);
	}

	printf(PRT_MSG"Colormap chunks... \n");

	for (int i = 0; i < CHUNKS_NY*CHUNKS_NX; i++) {
		pixelarray_setPixelsFrom1D(ingame_pixelarrays_relief[i], cmap_getColor_1D(cm_relief, ingame_pixelarrays_relief[i]->source_w *  ingame_pixelarrays_relief[i]->source_h, ingame_pixelarrays_relief[i]->source));
		pixelarray_combinePixelsFrom1D(ingame_pixelarrays_relief[i],cmap_getColor_1D(cm_height,    ingame_pixelarrays_dem[i]->source_w *  ingame_pixelarrays_dem[i]->source_h,     ingame_pixelarrays_dem[i]->source),0.5);
		pixelarray_free(ingame_pixelarrays_dem[i]);
	}

	printf(PRT_MSG"Register and arrange chunks... \n");

	// register pixel arrays in index order. Because the loading process is multi-threaded,
	// they won't always load in the right order, so we need to re-order them.
	// at this point we can also offset them from (0,0), which must be done based on their idx value, not loaded order
	cw = ingame_pixelarrays_relief[0]->width;			// width of chunk
	ch = ingame_pixelarrays_relief[0]->height;			// height of chunk
	fw = cw*CHUNKS_NX;
	fh = ch*CHUNKS_NY;
	int ix = 0;											// counter in x
	int iy = 0;											// counter in y
	int counter = 0;									// counter for indexing
	while (counter <= CHUNKS_NY*CHUNKS_NX-1){
		for (int i = 0; i < CHUNKS_NY*CHUNKS_NX; i++) {
			int this_order = ingame_pixelarrays_relief[i]->order;
			if (this_order == counter){

				// printf(PRT_MSG"Registering pixelarray with index %d. Position (%g, %g). \n",this_order,ix*cw,iy*ch);
				gamestate_registerPixelarray(ingame_state, ingame_pixelarrays_relief[i],10);

				pixelarray_setX(ingame_pixelarrays_relief[i],ix*cw);
				pixelarray_setY(ingame_pixelarrays_relief[i],iy*ch);

				if (ix < CHUNKS_NX-1) {
					ix += 1;
				} else {
					ix = 0;
					iy += 1;
				}
				
				counter++;
			}
		}
	}

	// Prepare minimap
	float minimap_p		 	= 7;		// Padding from corner of window (bottom right). Doesn't include border.
	float minimap_bw		= 2;		// Border width
	float minimapb_w 		= ingame_pixelarrays_relief[0]->source_w * CHUNKS_NX * MINIMAP_SCALE;
	float minimapb_h 		= ingame_pixelarrays_relief[0]->source_h * CHUNKS_NY * MINIMAP_SCALE;
	float minimapf_w 		= minimapb_w * windowWidth / fw;
	float minimapf_h 		= minimapb_h * windowHeight / fh;

	ingame_minimap_border 	= rectangle_init(gRenderer, windowWidth - minimapb_w - minimap_p - minimap_bw, windowHeight - minimapb_h - minimap_p - minimap_bw, minimapb_w + minimap_bw*2, minimapb_h + minimap_bw*2, color_bluegrey);
	gamestate_registerRectangle(ingame_state, ingame_minimap_border,21);

	ingame_minimap_back 	= rectangle_init(gRenderer, windowWidth - minimapb_w - minimap_p, windowHeight - minimapb_h - minimap_p, minimapb_w, minimapb_h, color_black);
	gamestate_registerRectangle(ingame_state, ingame_minimap_back,22);

	for (int i = 0; i < CHUNKS_NX*CHUNKS_NY;i++){
		cx_initial[i] = ingame_pixelarrays_relief[i]->x;
		cy_initial[i] = ingame_pixelarrays_relief[i]->y;
	}

	int w = (int) roundf(cw / fw * rectangle_getW(ingame_minimap_back));
	int h = (int) roundf(cw / fw * rectangle_getW(ingame_minimap_back));
	for (int i = 0; i < CHUNKS_NX*CHUNKS_NY; i++){
		float x = rectangle_getX(ingame_minimap_back) + cx_initial[i] * rectangle_getW(ingame_minimap_back) / fw;
		float y = rectangle_getY(ingame_minimap_back) + cy_initial[i] * rectangle_getH(ingame_minimap_back) / fh;

		ingame_minimap_chunks[i] = texture_createImitation(gRenderer, ingame_pixelarrays_relief[i]->mTexture, w, h);
		texture_setX(ingame_minimap_chunks[i],x);
		texture_setY(ingame_minimap_chunks[i],y);
		gamestate_registerTexture(ingame_state,ingame_minimap_chunks[i],23);
	}


	ingame_minimap_front 	= rectangle_init(gRenderer, windowWidth - minimapb_w - minimap_p, windowHeight - minimapb_h - minimap_p, minimapf_w, minimapf_h, color_white);
	ingame_minimap_front->filled = false;
	gamestate_registerRectangle(ingame_state, ingame_minimap_front,24);

	return true;

}

void gsf_ingame_logic() {

	int delta = (int) TRANS_RATE*deltaTime*powf(2,-1*cameraZ);

	cw = ingame_pixelarrays_relief[0]->width;
	ch = ingame_pixelarrays_relief[0]->height;
	fw = cw*CHUNKS_NX;
	fh = ch*CHUNKS_NY;
	float moveRestrict = 10; // How close the camera can get to the top and bottom of the world map

	hasMoved = false;

	if (keys[SDL_SCANCODE_A]) {
		for (int i = 0; i < CHUNKS_NY*CHUNKS_NX; i++) {
			pixelarray_transX(ingame_pixelarrays_relief[i],+delta);
		}
		hasMoved = true;
	}
	if (keys[SDL_SCANCODE_D]) {
		for (int i = 0; i < CHUNKS_NY*CHUNKS_NX; i++) {
			pixelarray_transX(ingame_pixelarrays_relief[i],-delta);
		}
		hasMoved = true;
	}
	if (keys[SDL_SCANCODE_W] && cameraY > moveRestrict) {
		for (int i = 0; i < CHUNKS_NY*CHUNKS_NX; i++) {
			pixelarray_transY(ingame_pixelarrays_relief[i],+delta);
		}
		hasMoved = true;
	}
	if (keys[SDL_SCANCODE_S] && -1*cameraY+fh>windowHeight+moveRestrict) {
		for (int i = 0; i < CHUNKS_NY*CHUNKS_NX; i++) {
			pixelarray_transY(ingame_pixelarrays_relief[i],-delta);
		}
		hasMoved = true;
	}

	if (hasMoved){
		cameraX = -1*ingame_pixelarrays_relief[0]->x;
		cameraY = -1*ingame_pixelarrays_relief[0]->y;

		// move minimap front rect
		rectangle_setX(ingame_minimap_front, rectangle_getX(ingame_minimap_back) + rectangle_getW(ingame_minimap_back) * cameraX / fw);
		rectangle_setY(ingame_minimap_front, rectangle_getY(ingame_minimap_back) + rectangle_getH(ingame_minimap_back) * cameraY / fh);

		// off left edge
		if (rectangle_getX(ingame_minimap_front) < rectangle_getX(ingame_minimap_back)){
			rectangle_setX(ingame_minimap_front, rectangle_getX(ingame_minimap_front) + rectangle_getW(ingame_minimap_back));
		}

		// cycle chunks periodically
		for (int i = 0; i < CHUNKS_NX*CHUNKS_NY; i++) {
			if (ingame_pixelarrays_relief[i]->x < -1*cw) {
				ingame_pixelarrays_relief[i]->x += fw;
			} else if (ingame_pixelarrays_relief[i]->x > windowWidth+cw) {
				ingame_pixelarrays_relief[i]->x -= fw;
			}
		}
	}

	if (cameraZ != cameraZ_old) {
		printf(PRT_MSG"Setting zoom to %d \n",cameraZ);

		// Scale chunks to new zoom
		cameraZ_old = cameraZ;
		float pa_sf = powf(2,cameraZ);
		float x_sf = ((float) windowWidth) / 2;
		float y_sf = ((float) windowHeight) / 2;

		for (int i = 0; i < CHUNKS_NY*CHUNKS_NX; i++) {
			pixelarray_setScaleAt(ingame_pixelarrays_relief[i], pa_sf, pa_sf, x_sf, y_sf);
		}

		// Rescale minimap selector
		rectangle_setW(ingame_minimap_front,  rectangle_getW(ingame_minimap_back) * windowWidth / fw);
		rectangle_setH(ingame_minimap_front,  rectangle_getH(ingame_minimap_back) * windowHeight / fh);
	}
	
	
}