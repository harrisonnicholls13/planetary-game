#include "planetary.h"
#include "globalvars.h"
#include "gamevars.h"

bool sourceFirst = true;
int loadPassFrame = 0;

bool mainLoop(){
	bool quit = false;

	// ----------------------------
	// HANDLE USER INPUTS
	// ----------------------------

	//Handle events on queue
	while( SDL_PollEvent( &e ) != 0 ) {
		// Requests quit (presses x button in window title bar)
		if( e.type == SDL_QUIT ) {
			quit = true;
		}
		// Key down event. Handles one key at a time, does not read multiple times if held for a short period.
		if( e.type == SDL_KEYDOWN ) {
			switch( e.key.keysym.sym ) {
				
				// Go to main menu if in-game.
				case SDLK_ESCAPE:
				if (gamestate_getCurrent()->ID == STATE_MAINMENU) {
					quit = true;
				} else {
					gamestate_setNext(STATE_MAINMENU);
					music_startf(mainmenu_music,-1);
				}
				break;

				case SDLK_F2:
				screenshot(gRenderer);
				break;

				case SDLK_F3:
				showDebug = !showDebug;
				break;

				case SDLK_F7:
				music_toggle();
				break;

				case SDLK_z:
				if (gamestate_getCurrent()->ID == STATE_INGAME){
					if (cameraZ < CAMERAZ_MAX) cameraZ++;
				}
				break;

				case SDLK_x:
				if (gamestate_getCurrent()->ID == STATE_INGAME){
					if (cameraZ > CAMERAZ_MIN) cameraZ--;
				}
				break;
				
				default:
				break;
			}
		} 

		// Update keys by scancode
		keys = (Uint8*) SDL_GetKeyboardState(NULL);

		// Update mouse coordinates
		if( e.type == SDL_MOUSEMOTION ) {
			SDL_GetMouseState( &mouseX, &mouseY );
		} 
		// Update mouse buttons on press
		if (e.type == SDL_MOUSEBUTTONDOWN ) {
			if (e.button.button == SDL_BUTTON_LEFT ) {
				mouse1 = 1;
			}
			if (e.button.button == SDL_BUTTON_RIGHT ) {
				mouse2 = 1;
			}
			if (e.button.button == SDL_BUTTON_MIDDLE ) {
				mouse3 = 1;
			}
		}
		// Update mouse buttons on release
		if (e.type == SDL_MOUSEBUTTONUP ) {
			if (e.button.button == SDL_BUTTON_LEFT ) {
				mouse1 = 0;
			}
			if (e.button.button == SDL_BUTTON_RIGHT ) {
				mouse2 = 0;
			}
			if (e.button.button == SDL_BUTTON_MIDDLE ) {
				mouse3 = 0;
			}
		}
	}

	// ----------------------------
	// HANDLE GAME STATES
	// ----------------------------


	// Go to new gamestate if requred, but running the current gamestate for 1 more update
	// Gives opportunity for buttons to be un-pressed, etc.
	if (_gamestate_next != _gamestate_current) {
		if (_gamestate_passed == true) {
			gamestate_makeCurrent(_gamestate_next);
			_gamestate_passed = false;
		} else {
			_gamestate_passed = true;
		}
	}

	// Update the current gamestate
	gamestate_update(gamestate_getCurrent());

	if (gamestate_getCurrent()->ID == STATE_INGAME) {
		gsf_ingame_logic();
	}

	// Pressed Quit button in mainmenu
	if (button_isClicked(mainmenu_button_quit)) {
		quit = true;
	}

	// Pressed Play button in mainmenu
	if (button_isClicked(mainmenu_button_new)) {
		if (!gamestate_isReady(STATE_INGAME)) {
			// Initialise game
			gamestate_setNext(STATE_LOADING);
		} else {
			// Game is already ready; go to it
			gamestate_setNext(STATE_INGAME);
			playlist_playRand(ingame_playlist);
		}
	}

	// Display loading screen for 4 frames before allowing loading to start, so that things can render.
	if (gamestate_getCurrent()->ID == STATE_LOADING){
		if (loadPassFrame != 3) {
			loadPassFrame++;																																		
		}
	}

	if (gamestate_getCurrent()->ID == STATE_LOADING && loadPassFrame == 3) {
		loadPassFrame = 0;

		// First time loading ingame state. Source PAs.
		if (sourceFirst){
			printf(PRT_MSG"First time loading planet. Sourcing from disk. \n");
			texture_updateText(loading_texture_status, "Reading planet from disk", font_small, color_white);
			for (int i = 0; i < CHUNKS_NY*CHUNKS_NX; i++){
				char chunk_path[STRLEN] =  "";
				snprintf(chunk_path, STRLEN, "%s%d.pa",ingame_pixelarrays_relief_path,i);
				pixelarray_loadSource(ingame_pixelarrays_relief[i], chunk_path);

				strcpy(chunk_path, "");

				snprintf(chunk_path, STRLEN, "%s%d.pa",ingame_pixelarrays_dem_path,i);
				pixelarray_loadSource(ingame_pixelarrays_dem[i], chunk_path);
			}
			sourceFirst = false;
		}

		// Calculate loaded percentage.
		float sum = 0;
		for (int i = 0; i < CHUNKS_NY*CHUNKS_NX; i++) {
			sum += ingame_pixelarrays_relief[i]->sourcepct;
			sum += ingame_pixelarrays_dem[i]->sourcepct;
		}

		loadingpct = (int) (sum / (2*CHUNKS_NX*CHUNKS_NY));

		char str[STRLEN];
		sprintf(str, "%d", loadingpct);
		strcat(str, "%");
		texture_updateText(loading_texture_percent, str, font_small, color_white);

		// Determine if all PAs are done loading from file.
		int ready = 0;
		for (int i = 0; i < CHUNKS_NY*CHUNKS_NX; i++) {
			ready += ingame_pixelarrays_relief[i]->hassource;
			ready += ingame_pixelarrays_dem[i]->hassource;
		}

		if (ready == (CHUNKS_NX*CHUNKS_NY*2) && !gamestate_isReady(STATE_INGAME)){
			texture_updateText(loading_texture_status, "Arranging chunks", font_small, color_white);

			for (int i = 0; i < CHUNKS_NY*CHUNKS_NX; i++) {

				if (ingame_pixelarrays_relief[i]->hassource == false) {
					printf(PRT_ERROR"False positive on ingame readiness. Order = %d (relief). Ready = %d. Sourced = %d \n", i,ready,ingame_pixelarrays_relief[i]->hassource);
					exit(EXIT_FAILURE);
				}

				if (ingame_pixelarrays_dem[i]->hassource == false) {
					printf(PRT_ERROR"False positive on ingame readiness. Order = %d (dem). Ready = %d. Sourced = %d \n", i,ready,ingame_pixelarrays_dem[i]->hassource);
					exit(EXIT_FAILURE);
				}
			}

			// Code below isn't multithreaded
			if (gsf_ingame_init()) {
				printf(PRT_MSG"Ingame init complete \n");
			} else {
				printf(PRT_ERROR"Ingame init failed \n");
				exit(EXIT_FAILURE);
			}

			texture_updateText(loading_texture_status, "Ready", font_small, color_white);

			gamestate_setReady(STATE_INGAME);
			gamestate_setNext(STATE_INGAME);
		}
	}

	// ----------------------------
	// CRITICAL UPDATE+RENDER TASKS
	// ----------------------------


	// Clear render
	SDL_SetRenderDrawColor(gRenderer, color_background.r, color_background.g, color_background.b, color_background.a ); // Background color
	SDL_RenderClear(gRenderer);
	
	// Render children of current gamestate
	gamestate_render(gamestate_getCurrent());

	// Render debug info
	if (showDebug) {
		dictbox_update(gRenderer, debugDictbox);
	}

	// Update screen
	SDL_RenderPresent(gRenderer);

	return !quit;
}

