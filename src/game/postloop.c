#include "planetary.h"
#include "gamevars.h"


// Things to do once the main loop exists
bool postLoop() {

	music_stop();

	dictbox_free(debugDictbox);
	cmap_free(cm_height);
	cmap_free(cm_relief);
	playlist_free(ingame_playlist);
	music_free(mainmenu_music);

	// Free registered gamestates and their children
	for (int i = _STATE_START+1; i < _STATE_END; i++) {
		if (gamestate_accountant[i] != NULL) {
			gamestate_free(i);
		}
	}

	return EXIT_SUCCESS;
}