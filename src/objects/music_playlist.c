#include "planetary.h"

/**
 * Music playlists are used to manage multiple music objects.
 * They allow you to play tracks in order, or randomise them.
 * It makes sense to have a playlist corresponding to a gamestate.
 */


/**
 * Initialise playlist structure, returning pointer to malloc'd memory
 */
playlist_t* playlist_init() {

	playlist_t* pl = (playlist_t*) malloc(sizeof(playlist_t));

	pl->music_pop = 0;
	pl->current = -1;

	return pl;
}

/**
 * Register previously music structure with a playlist
 * @param pl		playlist structure to associate with
 * @param ms		music structure to be associated 
 */
void playlist_registerMusic(playlist_t* pl, music_t* ms) {
	if (pl->music_pop < PLAYLIST_LEN - 1) {
		pl->musicPool[pl->music_pop] = ms;
		pl->music_pop += 1;
	} else {
		printf(PRT_WARN"Cannot register music with playlist ( %d / %d ) \n", pl->music_pop, PLAYLIST_LEN);
	}
}

/**
 * Play track from playlist structure
 * @param pl		playlist structure in question
 * @param i 		index of track within this playlist
 */
void playlist_playTrack(playlist_t* pl, int i) {
	if (i < pl->music_pop && i >= 0) { // check there's a track with this ID
		music_startf(pl->musicPool[i],0);
		pl->current = i;
	} else {
		printf(PRT_WARN"Track %d of playlist cannot be played because it is not registered \n", i);
	}
}

/**
 * Play next track from playlist structure, by index
 * @param pl		playlist structure in question
 */
void playlist_playNext(playlist_t* pl) {
	if (pl->music_pop>0) { // has at least one track registered
		if (pl->current != pl->music_pop-1) { // not on last track
			playlist_playTrack(pl, pl->current+1);
		} else { 
			playlist_playTrack(pl, 0);
		}
	} else {
		printf(PRT_WARN"Cannot play the next track of an empty playlist \n");
	}
}

/**
 * Play a random track from a playlist structure
 * @param pl		playlist structure in question
 */
void playlist_playRand(playlist_t* pl) {
	if (pl->music_pop > 0 ) {
		if (pl->music_pop == 1) {
			playlist_playTrack(pl, 0);
		} else {
			bool found = false;
			int i = 0;
			while (!found) {
				i = (int) roundf(getRand(0,pl->music_pop));
				found = (i != pl->current);
			}
			playlist_playTrack(pl, i);
		}
	} else {
		printf(PRT_WARN"Cannot play a random track from an empty playlist \n");
	}
	
}

void playlist_free(playlist_t* pl) {
	free(pl);
}
