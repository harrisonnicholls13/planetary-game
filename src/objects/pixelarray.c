#include "planetary.h"
#include "globalvars.h"
#include "gamevars.h"

/**
 * A pixel array is an array of pixel values that can be rendered using a streaming texture.
 * The pixel values are stored in a 1D array. The pixels are typically generated in the following way:
 * Read .pa file -> Creates 'source' array -> Colormap source -> Creates 'parent' array -> 
 * 		-> Crop parent to display region -> Creates 'pixels' array -> Convert pixels to texture
 * 		-> Render texture to gRenderer
 * PixelArrays were designed to display the surfaces of planets.
 */

/**
 * Initialise pixel array (returns pixel array pointer)
 * @param gRenderer SDL Renderer that the texture will be rendered by
 * @param width		width of PixelArray (unscaled)
 * @param height	height of PixelArray (unscaled)
 */
pixelarray_t* pixelarray_init(SDL_Renderer* gRenderer, int pix_width, int pix_height) {

	pixelarray_t* pa =  (pixelarray_t*) malloc(sizeof(pixelarray_t));
	if (pa == NULL) {
		printf(PRT_ERROR"Error allocating memory for PixelArray! \n");
		exit(EXIT_FAILURE);
	}

	pa->gRenderer	= gRenderer;
	pa->order		= -1;
	pa->x			= 0;
	pa->y			= 0;
	pa->pix_width 	= pix_width;
	pa->width		= pix_width;
	pa->scalex		= 1;
	pa->pix_height	= pix_height;
	pa->height		= pix_height;
	pa->scaley		= 1;
	pa->_pitch		= sizeof(pix_t) * pix_width;
	pa->_format		= SDL_PIXELFORMAT_ARGB8888;		// 0xAARRGGBB
	pa->_locked		= true; // lock texture to change pixels
	pa->hassource	= false;
	pa->thread		= NULL;
	pa->zoom 		= 1;
	pa->zoom_index  = 0;
	pa->sourcepct	= 0;

	pa->mTexture = SDL_CreateTexture(gRenderer, pa->_format, SDL_TEXTUREACCESS_STREAMING, pa->pix_width, pa->pix_height);
	if (pa->mTexture == NULL) {
		printf(PRT_ERROR"Error allocating memory for PixelArray texture! \n");
		exit(EXIT_FAILURE);
	}

	pa->pixels = (pix_t*) aligned_alloc(sizeof(pix_t), pix_width * pix_height* sizeof(pix_t));
	if (pa->pixels == NULL) {
		printf(PRT_ERROR"Error allocating memory for PixelArray pixels! \n");
		exit(EXIT_FAILURE);
	}

	memset(pa->pixels, 0xFFFF00FF, pa->pix_width * pa->pix_height * sizeof(pix_t));

	pixelarray_unlock(pa);

	return pa;
}


/**
 * Hidden function. Called by "pixelarray_loadSource()" when forking a new thread
 * @param ptr	Pointer to a "_pa_toThread" structure
 */
int _pixelarray_loadSourceTHREAD(void* ptr) {

	pixelarray_t* pa = (pixelarray_t*) ptr;

	FILE* file = fopen(pa->sourcepath, "r");

	if (file == NULL) {
		printf(PRT_ERROR"Could not open PixelFile at '%s' \n", pa->sourcepath);
		exit(EXIT_FAILURE);
	}
	

	int tempW = 0;
	int tempH = 0;
	int order = -1;
	char paHeader[STRLEN];

	bool readError = false;
	readError = (fscanf(file,"%[^\n]\n",paHeader) == EOF); 	// line 1 header contains misc info
	readError = (fscanf(file,"%i\n",&order) == EOF); 		// line 2 contains index (for chunking)
	readError = (fscanf(file,"%i\n",&tempW) == EOF); 		// line 3 contains width information
	readError = (fscanf(file,"%i\n",&tempH) == EOF); 		// line 4 contains height information

	if (readError) {
		printf(PRT_WARN"Error reading file! \n");
	}

	printf(PRT_MSGT"%s. \t Order %d \t - Loading source. Dimensions (w, h) = (%d, %d). \n",paHeader,order, tempW, tempH );


	int area = tempW*tempH;

	// Allocate memory for whole parent array (1D)
	pa->source = (float*) aligned_alloc(sizeof(float), area * sizeof(float));
	if (pa->source == NULL) {
		printf(PRT_WARN"Could not allocate memory for parent array\n");
	}

	// Read file line by line (now that we have its dimensions)
	char line[STRLEN];
	for (int k = 0; k < area; k++) { // for each line in the file
		int result = fscanf(file,"%s\n",line); // read line from file

		if (result != EOF) {
			sscanf(line, "%g",&pa->source[k]); // convert output of line reading to float
		} else {
			printf(PRT_WARN"Done reading pixel file when dimensions suggest otherwise \n");
		}

		pa->sourcepct = (int) ((float) k / (float) area * 100);
	}

	pa->order = order;
	pa->source_w = tempW;
	pa->source_h = tempH;
	pa->width = tempW;
	pa->height = tempH;

	fclose(file); 

	pa->hassource = true;

	printf(PRT_MSGT"%s. \t Order %d. \t - Thread done. \n",paHeader,order);

	return 0;
}

/**
 * Load pixel values from file (as well as dimensions). Pixels stored as source of pixelarray.
 * @param pa		Previously initialised pixelarray structure
 * @param path		File to read from
 */
void pixelarray_loadSource(pixelarray_t* pa, char* path) {
	// if (access(path,F_OK) != -1) {
	// 	printf(PRT_MSG"Reading PixelFile at '%s' \n",path);
	// } 
	if (access(path,F_OK) != 0){
		printf(PRT_ERROR"PixelFile at '%s' doesn't exist \n", path);
		exit(EXIT_FAILURE);
	}
	if(access(path,R_OK) != 0){
		printf(PRT_ERROR"Insufficient permissions for reading PixelFile at '%s' \n", path);
		exit(EXIT_FAILURE);
	}

	strcpy(pa->sourcepath, path);

	pa->thread = SDL_CreateThread(_pixelarray_loadSourceTHREAD, "LoadSource_THREAD", (void*) pa);
}

/**
 * Load a 1D matrix of pixels into the pixel array, and updates the texture. Modifies the entire pixel array.
 * @param pa		PixelArray structure
 * @param arr		Array containing new pixels
 */
void pixelarray_setPixelsFrom1D(pixelarray_t* pa, pix_t* arr) {
	pixelarray_lock(pa);
	memcpy(pa->pixels, arr, pa->_pitch*pa->pix_height);
	pixelarray_unlock(pa);
}

/**
 * Combine existing pixels with external pixel array by ratio
 * @param pa		PixelArray structure
 * @param arr		Array containing external pixels
 * @param ratio		Combination ratio. 
 */
void pixelarray_combinePixelsFrom1D(pixelarray_t* pa, pix_t* arr,float ratio) {
	pixelarray_lock(pa);
	for (int i = 0; i < pa->pix_width*pa->pix_height; i++){
		pa->pixels[i] = cmap_combinePixels0D(pa->pixels[i],arr[i],ratio);
	}
	pixelarray_unlock(pa);
}

/**
 * Lock pixelarray texture, so pixels can be updated
 * @param pa		PixelArray structure
 */
void pixelarray_lock(pixelarray_t* pa) {
	if( pa->_locked == false) {
		SDL_LockTexture(pa->mTexture, NULL, (void**)&pa->pixels, &pa->_pitch);
		pa->_locked = true;
	}
}

/**
 * Unlock pixelarray texture for rendering
 * @param pa		PixelArray structure
 */
void pixelarray_unlock(pixelarray_t* pa) {
	if( pa->_locked == true ) {
		SDL_UnlockTexture( pa->mTexture );
		pa->_locked = false;
	}
}

/**
 * Set blend mode of PixelArray; enables alpha, etc.
 * @param pa		PixelArray structure
 * @param blending	Blend mode
 */
void pixelarray_setBlendMode(pixelarray_t* pa, SDL_BlendMode blending ) {
	SDL_SetTextureBlendMode(pa->mTexture, blending );
}

/**
 * Modulate alpha value of PixelArray (transparency)
 * @param pa		PixelArray structure
 * @param alpha		Alpha value
 */
void pixelarray_setAlpha(pixelarray_t* pa, Uint8 alpha ) {
	SDL_SetTextureAlphaMod( pa->mTexture, alpha );
}

/**
 * Set individual pixel values in PixelArray
 * @param pa	pixel array structure
 * @param x		x coordinate within PixelArray
 * @param y		y coordinate within PixelArray
 * @param value	value to set pixel to
 */
void pixelarray_setPixel(pixelarray_t* pa, int x, int y, pix_t value) {
	pixelarray_lock(pa);
	pa->pixels[x + y*pa->pix_width] = value;
	pixelarray_unlock(pa);
}

/**
 * Set all of the pixels within a PixelArray to a value
 * @param pa	Pixel array structure
 * @param value	Value to set pixel to
 */
void pixelarray_setAll(pixelarray_t* pa, pix_t value) {
	// memset(pa->pixels, value, pa->pix_width * pa->pix_height * sizeof(pix_t));
	pixelarray_lock(pa);
	for (int i = 0; i< (pa->pix_width * pa->pix_height); i++) {
		pa->pixels[i] = value;
	}
	pixelarray_unlock(pa);
}

void pixelarray_setX(pixelarray_t* pa, float x) {
	pa->x = x;
}

void pixelarray_setY(pixelarray_t* pa, float y) {
	pa->y = y;
}

void pixelarray_transX(pixelarray_t* pa, float dx) {
	pa->x += dx;
}

void pixelarray_transY(pixelarray_t* pa, float dy) {
	pa->y += dy;
}

void pixelarray_print(pixelarray_t* pa) {
	for (int i = 0; i < (pa->pix_height*pa->pix_width); i++) {
		printf(PRT_MSG"Pixel %d = 0x%x \n", i, pa->pixels[i]);
	}
}

Uint32 pixelarray_getFormat(pixelarray_t* pa) {
	return pa->_format;
}

/**
 * Scales PixelArray about its origin (top left)
 * @param pa		PixelArray structure
 * @param scalex	Scale factor in x-direction
 * @param scaley	Scale factor in y-direction
 */
void pixelarray_setScale(pixelarray_t* pa, float scalex, float scaley) {
	
	if (scalex < TEXTURE_S_MIN) scalex = TEXTURE_S_MIN;
	if (scaley < TEXTURE_S_MIN) scaley = TEXTURE_S_MIN;

	pa->scalex = scalex;
	pa->scaley = scaley;

	pa->width  = (int) (pa->pix_width  * scalex);
	pa->height = (int) (pa->pix_height * scaley);
}

/**
 * Scales PixelArray about the position (u,v) such that it expands from that point, by translating as required
 * @param pa		PixelArray structure
 * @param scalex	Scale factor in x-direction
 * @param scaley	Scale factor in y-direction
 * @param u			Origin of scale transformation (x-dimension)
 * @param v			Origin of scale transformation (y-dimension)
 */
void pixelarray_setScaleAt(pixelarray_t* pa, float scalex, float scaley, float u, float v) {

	float old_width  = (float) pa->width;
	float old_height = (float) pa->height;

	if (scalex < TEXTURE_S_MIN) scalex = TEXTURE_S_MIN;
	if (scaley < TEXTURE_S_MIN) scaley = TEXTURE_S_MIN;

	pa->scalex = scalex;
	pa->scaley = scaley;

	pa->width  = (int) (pa->pix_width  * scalex);
	pa->height = (int) (pa->pix_height * scaley);

	// Set new (x,y) coordinates for texture such that we zoom in on (u,v)
	pa->x = u + (pa->pix_width  * scalex / old_width)  * (pa->x - u);
	pa->y = v + (pa->pix_height * scaley / old_height) * (pa->y - v);

}

/**
 * Free memory being used by PixelArray
 * @param pa	pixel array structure
 */
void pixelarray_free(pixelarray_t* pa) {
	SDL_DestroyTexture(pa->mTexture);
	// free(pa->pixels);
	free(pa);
}

/**
 * Render pixel array
 * @param pa	pixel array structure
 */
void pixelarray_render(pixelarray_t* pa) {
	SDL_Rect renderQuad = { (int) roundf(pa->x) , (int) roundf(pa->y), pa->width, pa->height };
	SDL_RenderCopyEx( pa->gRenderer, pa->mTexture, NULL, &renderQuad, 0, NULL, SDL_FLIP_NONE );
}
