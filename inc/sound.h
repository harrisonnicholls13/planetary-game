#include "planetary.h"

typedef struct sd {

	Mix_Chunk 	*chunk;
	int 		channel;

} sound_t;

sound_t* sound_loadFromFile(char* file);
void sound_free(sound_t* sd);
void sound_play(sound_t* sd,int loopNumber);