#include "planetary.h"

// Time structure
typedef struct tim {
		
		Uint32	mStartTicks; //Clock time when the timer started
		Uint32	mPausedTicks; //Ticks stored when the timer was paused
		bool	mPaused; //Timer status
		bool	mStarted; //Timer status
	
} sdlTimer_t;


// Prototype functions
sdlTimer_t* timer_init();
void	timer_start(sdlTimer_t* tim);
void	timer_stop(sdlTimer_t* tim);
void	timer_pause(sdlTimer_t* tim);
void	timer_unpause(sdlTimer_t* tim);
Uint32	timer_getTicks(sdlTimer_t* tim);
bool	isStarted(sdlTimer_t* tim);
bool	isPaused(sdlTimer_t* tim);


