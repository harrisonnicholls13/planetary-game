#include "planetary.h"


typedef struct pl {

	music_t*	musicPool[PLAYLIST_LEN];
	int			music_pop;
	int 		current;


} playlist_t;


playlist_t* 	playlist_init();
void 			playlist_free(playlist_t* pl);
void 			playlist_registerMusic(playlist_t* pl, music_t* ms);
void 			playlist_playTrack(playlist_t* pl, int i);
void 			playlist_playNext(playlist_t* pl);
void 			playlist_playRand(playlist_t* pl);