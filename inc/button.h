#include "planetary.h"

typedef struct bt {
	// Parameters
	int 	x;				// x coordinate
	int		y;				// y coordinate
	int		w;				// width (pixels)
	int		h;				// height (pixels)
	int		b;				// thickness of border (pixels)
	char	text[STRLEN];	// text in button
	bool	_held;			// is set to true when held down
	bool	_clicked;		// is set to true when the button is first clicked
	bool	_hovered;		// is set to true when hovered over (continuous)
	bool	_released;		// is set to true when released on this update
	bool 	_newhover; 		// is set to true when started hovering on this update

	// 'Decorative' things
	SDL_Renderer* gRenderer;
	TTF_Font*	gFont;
	SDL_Color	borderColor;
	SDL_Color 	borderColorAlt;
	SDL_Color	foregroundColor;
	SDL_Color	backgroundColor;
	SDL_Color	backgroundColorAlt;
	texture_t* 	texture;

	// Pre-calculated values to store, for optimisation purposes
	int px;		// padding in x direction (to centre the text)
	int py;		// padding in y direction (to centre the text)

} button_t;

button_t*	button_init(SDL_Renderer* gRenderer,  int x, int y, char* text, TTF_Font* gFont);
void 		button_update(button_t* bt, int mouseX, int mouseY, int mouseState);
void 		button_render(button_t* bt);
void		button_free(button_t* bt);
void 		button_setText(button_t* bt, char* text);
bool		button_isHeld(button_t* bt);
bool		button_isClicked(button_t* bt);
bool 		button_isHovered(button_t* bt);
bool 		button_isNewHover(button_t* bt);

