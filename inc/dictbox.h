#include "planetary.h"

typedef struct db {
	// Params
	int x;  // Offset from origin
	int y;  // Offset from origin
	int b;  // Border thickness
	int p;  // Internal padding thickness
	int s;  // Spacing between lines
	TTF_Font* gFont;
	SDL_Color borderColor;
	SDL_Color foregroundColor;
	SDL_Color backgroundColor;

	// Calculated params
	int w;
	int h;
	int lines;
	int current_pop;

	// Things to display
	char** labels;
	int** values;

	// Textures
	texture_t** textures;
	
} dictbox_t;

dictbox_t*	dictbox_init(int lines, int fixedW, int x, int y, TTF_Font* gFont, SDL_Color borderColor, SDL_Color foregroundColor, SDL_Color backgroundColor);
void 		dictbox_update(SDL_Renderer* gRenderer, dictbox_t* tb);
void 		dictbox_setLine(dictbox_t* db, char* label, int* value_ptr);
void 		dictbox_free(dictbox_t* db);


