#include "planetary.h"

typedef struct conf {
  int	windowWidth;
  int	windowHeight;
  char	windowTitle[STRLEN];
  int	vsync;
  int	fpsMax;

} config_t;

config_t* config_read(char* path);
