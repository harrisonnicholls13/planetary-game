#include "planetary.h"

typedef Uint32 pix_t;

// Structure used by "loadSourceTHREAD" to pass both pa and path at the same time

typedef struct pa {
	
	int 	order;		// index for this PA when part of a chunked map

	int 	pix_width; 	// original width
	int 	pix_height; // original height
	int 	_pitch;		// the number of bytes that make up a single row of pixels
	int 	width;		// scaled width
	int 	height; 	// scaled height
	float	scalex;		// SF in xhat
	float	scaley; 	// SF in yhat
	float 	x;			// x position
	float 	y;			// y position
	float 	zoom;		// pixel zoom level
	int		zoom_index; // index used to calculate zoom level (zoom = 2^zoom_index)
	bool 	_locked;	// Texture locked for rendering?

	SDL_Renderer*	gRenderer;
	SDL_Texture*	mTexture;
	Uint32 			_format;
	SDL_Thread*		thread;

	pix_t* 	pixels;			// Array containing currently rendered pixels

	char	sourcepath[STRLEN];
	float*	source;			// Array of source pixels (before cmap)
	int		source_w;		// Width of source
	int		source_h;		// Height of source
	bool 	hassource;
	int		sourcepct;		// loaded % of source

} pixelarray_t;


// Init functions
pixelarray_t*	pixelarray_init(SDL_Renderer* gRenderer, int pix_width, int pix_height);
void			pixelarray_loadSource(pixelarray_t* pa, char* path);
void			pixelarray_free(pixelarray_t* pa);

// Geometry set functions. Changes the PA on a texture-level, not on a pixel-level.
void	pixelarray_setX(pixelarray_t* pa, float x);
void	pixelarray_transX(pixelarray_t* pa, float dx );
void	pixelarray_setY(pixelarray_t* pa, float y);
void	pixelarray_transY(pixelarray_t* pa, float dy );
void	pixelarray_setBlendMode(pixelarray_t* pa, SDL_BlendMode blending ); 
void	pixelarray_setAlpha(pixelarray_t* pa, Uint8 alpha );
void	pixelarray_setScale(pixelarray_t* pa, float scalex, float scaley);
void	pixelarray_setScaleAt(pixelarray_t* pa, float scalex, float scaley, float u, float v);

// Pixel manipulation functions. Changes pixel structure within the array.
void 	pixelarray_setPixel(pixelarray_t* pa, int x, int y, pix_t value);
void 	pixelarray_setAll(pixelarray_t* pa, pix_t value);
void 	pixelarray_setPixelsFrom1D(pixelarray_t* pa, pix_t* arr);
void 	pixelarray_combinePixelsFrom1D(pixelarray_t* pa, pix_t* arr,float ratio);
void 	pixelarray_lock(pixelarray_t* pa);
void 	pixelarray_unlock(pixelarray_t* pa);

// Rendering functions
void 	pixelarray_render(pixelarray_t*);

// Evaluated functions
int 	pixelarray_getZoom(pixelarray_t* pa);
Uint32 	pixelarray_getFormat(pixelarray_t* pa);
void 	pixelarray_print(pixelarray_t* pa);