#include "planetary.h"

typedef struct cm {

	int 		sample_size;		// Number of vals to sample with
	SDL_Color*	rgba_colors;		// Array containing sample colours (rgba)
	float* 		values;				// Values [0,1] that the colours are mapped to
	Uint32 		_format;			// Pixel format used
	int 		interpolation;		// Interpolation method (0 = none, 1 = nearest, 2 = average, 3 = squared average)
	bool 		reversed;

} cmap_t;

// Prototype functions...

cmap_t* 	cmap_init(int scheme, bool reversed);
pix_t 		cmap_getColor(cmap_t* cm, float value);
pix_t* 		cmap_getColor_1D(cmap_t* cm, int len, float array[len]);
void 		cmap_setInterpolation(cmap_t* cm, int interp);
void 		cmap_free(cmap_t* cm);

pix_t cmap_combinePixels0D(pix_t a, pix_t b,float ratio);
