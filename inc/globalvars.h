#include "planetary.h"

extern 		 SDL_Window* gWindow;
extern 		 SDL_Renderer* gRenderer;

extern 		 TTF_Font* font_small;
extern 		 TTF_Font* font_big;
extern const SDL_Color color_background; 
extern const SDL_Color color_white;
extern const SDL_Color color_black; 
extern const SDL_Color color_white_50; 
extern const SDL_Color color_blue;
extern const SDL_Color color_green;
extern const SDL_Color color_red;
extern const SDL_Color color_bluegrey;
extern const SDL_Color color_bluewhite;

extern       int 		windowWidth;
extern       int 		windowHeight;
extern 		 int 		vsync;
extern const int 		fpsMax;
extern const char* 		windowTitle;
extern 		 Uint32 	TPF;

extern		 int		intFPS;
extern		 Uint32		deltaTime;

extern		 SDL_Event 	e;
extern		 int		mouseX;
extern		 int 		mouseY;
extern 		 int 		mouse1;
extern 		 int 		mouse2;
extern 		 int 		mouse3;
extern 		 Uint8* 	keys;

