#include "planetary.h"

// Game state management
extern int				_gamestate_current;
extern int				_gamestate_previous;
extern int				_gamestate_next;
extern bool 			_gamestate_passed;
extern gamestate_t* 	gamestate_accountant[_STATE_END+1];
 
// Sound effects
extern sound_t*			buttonpress_sound;
extern sound_t*			buttonhover_sound;
extern music_t*			mainmenu_music;
extern playlist_t*		ingame_playlist;

// Main menu
extern gamestate_t* 	mainmenu_state;
extern button_t*		mainmenu_button_new;
extern button_t*		mainmenu_button_load;
extern button_t*		mainmenu_button_settings;
extern button_t*		mainmenu_button_quit;
extern texture_t*		mainmenu_texture_background;
extern texture_t*		mainmenu_texture_title;
extern texture_t*		mainmenu_texture_author;

// Loading screen
extern gamestate_t*		loading_state;
extern texture_t*		loading_texture_background;
extern texture_t*		loading_texture_message;
extern texture_t*		loading_texture_status;
extern texture_t*		loading_texture_percent;
extern int				loadingpct;

// In game
extern gamestate_t* 	ingame_state;
extern pixelarray_t*	ingame_pixelarrays_relief[CHUNKS_NY*CHUNKS_NX];
extern char*			ingame_pixelarrays_relief_path;

extern pixelarray_t*	ingame_pixelarrays_dem[CHUNKS_NY*CHUNKS_NX];
extern char*			ingame_pixelarrays_dem_path;

extern texture_t*		ingame_minimap_chunks[CHUNKS_NY*CHUNKS_NX];

extern float 			cx_initial[CHUNKS_NX*CHUNKS_NY];
extern float 			cy_initial[CHUNKS_NX*CHUNKS_NY];

extern rectangle_t*		ingame_minimap_back;
extern rectangle_t*		ingame_minimap_front;
extern float			fw;
extern float			fh;
extern float			cw;
extern float			ch;

extern cmap_t*			cm_height;
extern cmap_t*			cm_relief;
extern cmap_t*			cm_ocean;

extern float			cameraX;
extern float			cameraY;
extern int				cameraZ;

// Different objects to handle
extern texture_t* 		init_text;

// Debug variables
extern bool 			showDebug;
extern dictbox_t* 		debugDictbox;