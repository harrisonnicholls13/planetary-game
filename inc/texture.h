#include "planetary.h"

typedef struct tex {
	int 	pix_width; 	// original width
	int 	pix_height; // original height
	int 	width;		// scaled width
	int 	height; 	// scaled height
	float	scalex;		// SF in xhat
	float	scaley; 	// SF in yhat
	float 	x;			// x position
	float 	y;			// y position
	double 	angle;		// rotation angle (degrees, clockwise)

	int 	tex_type; 	// 0 = image ; 1 = word

	SDL_Renderer* gRenderer;
	SDL_Texture* mTexture;

	SDL_Rect* clip;
	SDL_RendererFlip flip;

} texture_t;



// Texture prototype functions
texture_t*	texture_loadFromFile(SDL_Renderer* gRenderer, char* path );
texture_t*	texture_loadFromText(SDL_Renderer* gRenderer, char* textureText, TTF_Font* gFont, SDL_Color textColor );
texture_t* 	texture_createImitation(SDL_Renderer* gRenderer, SDL_Texture* mtex, int pix_w, int pix_h);
void		texture_free(texture_t* tex);

void	texture_setX(texture_t* tex, float x);
void	texture_transX(texture_t* tex, float dx );
void	texture_setY(texture_t* tex, float y);
void	texture_transY(texture_t* tex, float dy );
void	texture_setColor(texture_t* tex, SDL_Color col );
void	texture_setBlendMode(texture_t* tex, SDL_BlendMode blending ); 
void	texture_setAlpha(texture_t* tex, Uint8 alpha );
void 	texture_setClip(texture_t* tex, SDL_Rect* clip);
void 	texture_setAngle(texture_t* tex, double angle);
void 	texture_setFlip(texture_t* tex, SDL_RendererFlip flip);
void	texture_setScale(texture_t* tex, float scalex, float scaley);
void	texture_setScaleAt(texture_t* tex, float scalex, float scaley, float u, float v);
void 	texture_scaleToWindow(texture_t* tex);

void 	texture_updateText(texture_t* tex, char* text, TTF_Font* gFont, SDL_Color color);
void 	texture_renderClone(SDL_Renderer* gRenderer, SDL_Texture* mTexture, float x, float y, int w, int h);
void 	texture_render(texture_t* tex);

int 	texture_getWidth(texture_t* tex);
int 	texture_getHeight(texture_t* tex);
