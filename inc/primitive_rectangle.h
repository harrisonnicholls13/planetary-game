#include "planetary.h"

typedef struct rect {

	// Geometry stored as float, to avoid rounding errors
	float x;
	float y;
	float w;
	float h;

	// Objects to draw
	SDL_Renderer* gRenderer;
	SDL_Rect r;
	SDL_Color color;

	// Fill rectangle, or outline?
	bool filled;
	bool hide;
	
} rectangle_t;

rectangle_t* 	rectangle_init(SDL_Renderer* gRenderer, float x, float y, float w, float h, SDL_Color color);
void 			rectangle_free(rectangle_t* rect);
void 			rectangle_render(rectangle_t* rect);

void 			rectangle_setX(rectangle_t* rect, float x);
void 			rectangle_setW(rectangle_t* rect, float w);
void 			rectangle_setY(rectangle_t* rect, float y);
void 			rectangle_setH(rectangle_t* rect, float h); 
void 			rectangle_toggleHide(rectangle_t* rect);

float 			rectangle_getX(rectangle_t* rect);
float 			rectangle_getW(rectangle_t* rect);
float			rectangle_getY(rectangle_t* rect);
float			rectangle_getH(rectangle_t* rect);

void 			drawSDLRect(SDL_Renderer* gRenderer, float x, float y, float w, float h, bool isFilled);