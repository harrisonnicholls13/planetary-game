#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_thread.h>
#include <pthread.h>


// These could have been functions, but their low complexity and reliance on only boolean logic
// means that this is better, as it uses much less memory.
#define AVCHK(x) 		strcmp(argv[i],x)==0     // Does argument array contain string x?
#define I2C(x) 			(char)(x+'0')			 // Convert int to char (e.g.  4  -> '4')
#define C2I(x) 			(int)(x-'0')			 // Convert char to int (e.g. '9' ->  9 )#
#define GXFP(xp)		xp*windowWidth			 // Get x coord from window percentage
#define GYFP(yp)		yp*windowHeight			 // Get y coord from window percentage


// Prefixes to print messages (with colors!)
#define PRT_SPACE		"---------  "
#define PRT_MSG			"\033[0;32m[Message]  \033[0m"  // General message
#define PRT_MSGT		"\033[0;36m[Message]  \033[0m"  // Message from a thread
#define PRT_WARN		"\033[0;33m[Warning]  \033[0m"  // Warning 
#define PRT_ERROR		"\033[0;31m[ Error ]  \033[0m"  // Error


// Game variables (overriden by config file, if one is loaded)
// THESE ARE JUST DEFAULT VALUES!
#define DEFAULT_CONF	"./res/default.conf"	// Default location of configuration file
#define WINDOW_WIDTH 	1280					// Window width in pixels
#define WINDOW_HEIGHT	720						// Window height in pixels
#define WINDOW_TITLE	"Planetary game"		// Window title string
#define VSYNC 			0						// This takes precedence over the FPS_MAX limit
#define FPS_MAX			30						// To remove FPS limit just make this number really big


// Limits on variables and stuff
#define STRLEN 			512 					// Maximum length of a string (unless otherwise set)
#define FPS_ERRORABOVE	2000000					// if (avgFPS > FPS_ERRORABOVE) {avgFPS = 0};
#define	FPS_SAMPLE		30						// how many frames to average over when calculating FPS
#define TEXTURE_HOOF	true					// Texture Hide-Out-Of-Frame (don't render if outside the window)
#define TEXTURE_ENABLE	true					// Enable texture rendering
#define TEXTURE_CLIP	false					// Enable texture clipping (CRASHES GAME FOR SOME REASON)
#define TEXTURE_S_MIN	0.1						// Minimum texture scale
#define SOUND_RESET		false					// Force sound to restart if re-called prior to finishing

// Game related stuff
#define TRANS_RATE		2
#define CAMERAZ_MIN		0
#define CAMERAZ_MAX		10
#define MAX_BUTTONS		16
#define MAX_TEXTURES	256
#define MAX_SOUNDS		16
#define MAX_PIXELARRAYS	256
#define MAX_RECTANGLES	16
#define MAX_LAYERS		99
#define PLAYLIST_LEN	8
#define SCREENSHOTS		"./res/screenshots/"	// Folder to save screenshots to
#define MINIMAP_SCALE	0.02
#define CHUNKS_NX		20
#define CHUNKS_NY		10
#define CHUNKS_L		1000

// -------------------------------
// vvvvvvvvvvv GUARDED vvvvvvvvvvv
// -------------------------------

#ifndef GUARD
#define GUARD

#include "texture.h"
#include "primitive_rectangle.h"
#include "pixelarray.h"
#include "timer.h"
#include "dictbox.h"
#include "config.h"
#include "button.h"
#include "cmap.h"
#include "sound.h"
#include "music.h"
#include "music_playlist.h"
#include "gamestate.h"


// Prototype functions
void 	init();
void 	preLoop();
bool 	mainLoop();
bool 	postLoop();

float 	getRand(float min, float max);
float* 	linspace(float min, float max, int n);
void 	flatten2D(pix_t* out, pix_t** src, int w, int h);
void 	unflatten2D(pix_t** out, pix_t* src, int w, int h);
void 	weightedSum2D(pix_t* A, pix_t* B, int len, int w1, int w2);
void 	screenshot(SDL_Renderer* gRenderer);

enum 	cmap_interp 	{ _INTERP_START, CMAP_INTERP_NONE , CMAP_INTERP_NEAREST , CMAP_INTERP_AVERAGE , CMAP_INTERP_LINEAR , _INTERP_END };
enum	cmap_schemes   	{ _SCHEME_START, CMAP_SCHEME_MARS , CMAP_SCHEME_OCEAN, CMAP_SCHEME_GREYS , CMAP_SCHEME_COOLWARM , CMAP_SCHEME_TERRAIN , CMAP_SCHEME_EARTH, _SCHEME_END };
enum	gamestates		{ _STATE_START, STATE_MAINMENU , STATE_LOADING , STATE_INGAME , STATE_SETTINGS , STATE_OTHER , _STATE_END };

#endif