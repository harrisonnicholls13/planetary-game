#!/usr/bin/env python

from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import time
import progressbar
import os

Image.MAX_IMAGE_PIXELS = 93312000000

path = "/home/harrison/programming/planetary-game/res/planet_maps/mars/"

tiles_wide = 10
tiles_tall = 5

fig = plt.figure(figsize=(20,10))
ax = []

k = 0
bar = progressbar.ProgressBar(max_value=tiles_tall*tiles_wide)

for i in range(0,tiles_tall): # for each row
	for j in range(0,tiles_wide): # for each element in this row

		file = str(k) + ".pa"
		p = path + "./gen/" + file

		ax.append(fig.add_subplot(tiles_tall,tiles_wide,k+1))
		ax[k].get_xaxis().set_visible(False)
		ax[k].get_yaxis().set_visible(False)

		f = open(p,"r")
		f.readline()			# Header
		idx = int(f.readline())	# Index
		w = int(f.readline())	# width
		h = int(f.readline())	# height

		print("\nChunk idx = " + str(idx) + ". Dims = (" + str(w) + ", " + str(h) + ")")

		data = np.ones((h,w))

		x = 0
		y = 0
		for _ in range(w*h):
			line = f.readline()
			data[y][x] = float(line)

			if (x < w-1):
				x = x + 1
			else:
				x = 0
				y = y + 1

		f.close()

		ax[k].imshow(data,plt.get_cmap('gray'),interpolation='nearest',vmin=0,vmax=1)

		k = k + 1
		bar.update(k)
		

plt.savefig("out.png",dpi=600)
plt.tight_layout()
plt.show()

print("Done!")