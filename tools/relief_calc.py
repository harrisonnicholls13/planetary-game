#!/usr/bin/env python

from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import time
import progressbar
import os

Image.MAX_IMAGE_PIXELS = 93312000000


# Parameters

path = "/home/harrison/programming/planetary-game/res/planet_maps/mars/"
filename = "dem_20k.tif"

top_crop = 3000
bot_crop = 3000

downsample = 4
dp = 4

doplot = False

# Load data from image

print("Loading input image...")

im_file = Image.open(path + filename)
im_all = np.array(im_file)

im_all = np.delete(im_all,range(0,top_crop),axis=0)
im_all = np.delete(im_all,range(len(im_all)-bot_crop,len(im_all)),axis=0)
im_all = im_all[::downsample,::downsample]

im_min = np.amin(im_all)
print("min:" + str(im_min))
im_max = np.amax(im_all)
print("max:" + str(im_max))

im_all = np.around((im_all - im_min) / im_max,dp)

im_all_T = np.transpose(im_all)

width = len(im_all[0])
height = len(im_all_T[0])


# Plot input

if (doplot):

	print("Plot input...")

	fig = plt.figure(figsize=(20,10))
	ax = fig.add_subplot(111)

	ax.imshow(im_all,plt.get_cmap('gray'),interpolation='nearest',vmin=0,vmax=1)

	plt.tight_layout()
	plt.show()

# Calculate relief map...

'''
PLAN BELOW
Assume sun is long boi that wraps around the planet

_____________________________________
	S
											z
				.______.					^
				|	   |					|
				|      |					----> x
________________|______|_____________				
	0				x2		x1		

Pseudocode:

illuminate = 10

for each y:   						# Each column

	for each x1 in y:				# Points x1 in each column

		draw ray between S and x1
		max_diff = 0				# Assume no shadow by default

		for each x2 from 0 to x1:	# Points x2 between 0 and x1

			ray_height = height(ray at x2)
			ter_height = height(terrain at x2)
			diff = ter_height - ray_height

			if diff < 0:
				no shadow at x1
			else if diff > max_diff
				max_diff = diff
			else
				casts shadow but other terrain has larger contribution.

		illumination at x1 = illuminate * (1 - max_diff / tallest mountain)

'''

print("Calculating relief map...")

# Parameters
sun_x = -1
sun_z = 0.01
I_max = 1
world_height = 1

#  Store results here
relief_all = []

# use transpose to make easier
# first dimension is 'columns', which is what we treat individually

dim_x = height
dim_y = width

print(dim_x,dim_y)

# can pre-calculate rays because they are shared for column N in each row due to square geometry of map
# only need to do terrain vs ray calculation uniquely

k = 0
bar = progressbar.ProgressBar(max_value=dim_y)
for row in range(dim_y):			# For each row
	# print(" Row " + str(row) + " ------------------------------")
	rowvals = []
	
	for x1 in range(dim_x):			# For each pixel in row
		m = (im_all_T[x1,row] - sun_z) / (x1 - sun_x)
		c = sun_z
		max_diff = 0
		for x2 in range(sun_x,x1):	# For each pixel between sun and x1
			ray_z = m*x2 + c
			ter_z = im_all_T[x2,row]

			# print(ray_z,ter_z)
			diff = ter_z - ray_z

			if (diff > max_diff):
				max_diff = diff
				# print(diff)
				# print("\n")
				# exit()
				
		I_at_x1 = I_max * (1 - max_diff / world_height)
		rowvals.append(I_at_x1)
	bar.update(k)
	k = k + 1
	# print(rowvals)
	relief_all.append(rowvals)


print("Done!")




